var snow = {
	//variables
	width: window.innerWidth,height: window.innerHeight,flakeCount: 250,
	gravity: 0.7,windSpeed: 20,flakes: [],
	currentFlake: 0,snowGlobe: document.getElementById('snowglobe'),
	//methods
	getRandom: function(min,max){
		return Math.floor(Math.random() * (max - min) + min);
	},
	animate: function(){
		//while loop
		while (this.currentFlake < this.flakeCount){
			var flake = document.createElement('div'),
			newFlake;
			flake.className = "flake";
			flake.style.fontSize = this.getRandom(20,50) + 'px';
			flake.style.top = this.getRandom(0,this.height) + 'px';
			flake.style.left = this.getRandom(0,this.width) + 'px';
			flake.innerHTML = "*";
			newFlake = this.snowGlobe.appendChild(flake);
			//set a speed property to the newflake obj
			newFlake.speed = this.getRandom(1,100);
			this.flakes.push(newFlake);
			this.currentFlake++;
		}
		var flakes = this.flakes;
		//use the array of dom elements
		for(var i = 0; i < flakes.length; i++){
			positionX = false;
			positionY = false;
			//new Y position
			positionY = parseFloat(flakes[i].style.top) + (flakes[i].speed / 100);
			if(positionY > this.height){
				positionY = 0 - parseInt(flakes[i].style.fontSize);
				positionX = this.getRandom(0, this.width);
			}
			//new X position
			if (!positionX) positionX = parseFloat(flakes[i].style.left) + Math.sin(positionY / this.windSpeed);
	        if (positionX < 0) positionX += this.width;
	        if (positionX > this.width + 20) positionX -= this.width;
	        // Set new position
	        flakes[i].style.top = positionY + 'px';
		}
		window.requestAnimationFrame( (function(){ this.animate() }).bind(this) );
	}
}
snow.animate();
//check browser window, did it resize?
window.onresize = function(){
	snow.width = window.innerWidth;
	snow.height = window.innerHeight
	console.log(snow.width);
	console.log(snow.height);
}

