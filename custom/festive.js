function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var width = window.innerWidth;
var height = 650;
var flakeCount = 12;
var gravity = 0.7;
var windSpeed = 20;
var flakes = [];

var currentFlake = 0;
var snowglobe = document.getElementById("snowglobe");

while (currentFlake < flakeCount) {
    var flake = document.createElement("div");
    flake.className = 'flake';
    flake.style.fontSize = getRandom(40,70) + 'px';
    flake.style.top = getRandom(0, height) + 'px';
    flake.style.left = getRandom(0, width) + 'px';
    flake.innerHTML = "•";
    newFlake = snowglobe.appendChild(flake);
    newFlake.speed = getRandom(1, 100);
    flakes.push(newFlake);
    currentFlake++;
}

function doAnimation() {
    for (var i = 0; i < flakes.length; i++) {
        newX = false;
        newY = false;
        // Calculate Y position
        newY = parseFloat(flakes[i].style.top) + (flakes[i].speed / 100) * gravity;
        if (newY > height) {
            newY = 0 - parseInt(flakes[i].style.fontSize);
            // If Y is at bottom, randomize X
            newX = getRandom(0, width);
        }
        // Calculate X position if it hasn't been set randomly
        if (!newX) newX = parseFloat(flakes[i].style.left) + Math.sin(newY / windSpeed);
        if (newX < -20) newX = width + 20;
        if (newX > width + 20) newX = -20;
        // Set new position
        flakes[i].style.top = newY + 'px';
        flakes[i].style.left = newX + 'px';
    }
}


setInterval(doAnimation, 10);

window.onresize = function(event) {
	width = innerWidth;
}
